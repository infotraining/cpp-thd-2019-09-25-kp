#include <cassert>
#include <chrono>
#include <functional>
#include <iostream>
#include <string>
#include <thread>
#include <vector>
#include "joining_thread.hpp"

using namespace std::literals;

void may_throw(size_t id, size_t arg)
{
    if (arg == 13)
        throw std::runtime_error("Error#13 " + std::to_string(id));
}

void background_work(size_t id, const std::string& text,
                     std::chrono::milliseconds delay, std::exception_ptr& excpt)
{
    std::cout << "bw#" << id << " has started..." << std::endl;

    for (auto [it, index] = std::tuple(begin(text), 0u); it != end(text); ++it, ++index)
    {
        std::cout << "bw#" << id << ": " << *it << std::endl;

        try
        {
            may_throw(id, index);
        }
        catch(...)
        {
            excpt = std::current_exception();
            return;
        }

        std::this_thread::sleep_for(delay);
    }

    std::cout << "bw#" << id << " is finished..." << std::endl;
}

int main()
{
    std::cout << "Main thread starts..." << std::endl;

    std::vector<std::exception_ptr> excpts(2);

    {
        ext::joining_thread thd1{&background_work, 42, "text", 100ms, std::ref(excpts[0])};
        ext::joining_thread thd2{&background_work, 665, "textdfjgskjhfgasdjhkfgahkjsgfka", 100ms, std::ref(excpts[1])};
    }

    for( const auto& e : excpts)
    {
        if (e)
        {
            try
            {
                std::rethrow_exception(e);
            }
            catch (const std::runtime_error& e)
            {
                std::cout << "Caught an exception: " << e.what() << std::endl;
            }
        }
    }

    std::cout << "Main thread ends..." << std::endl;
}
