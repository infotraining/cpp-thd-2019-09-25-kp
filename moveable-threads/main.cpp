#include <cassert>
#include <chrono>
#include <functional>
#include <iostream>
#include <string>
#include <thread>
#include <vector>

using namespace std::literals;

void hello()
{
    std::string text = "Hello Concurrent World";

    for (const auto& c : text)
    {
        std::cout << c << " ";
        std::this_thread::sleep_for(100ms);
        std::cout.flush();
    }

    std::cout << std::endl;
}

void background_work(size_t id, const std::string& text, std::chrono::milliseconds delay)
{
    std::cout << "bw#" << id << " has started..." << std::endl;

    for (const auto& c : text)
    {
        std::cout << "bw#" << id << ": " << c << std::endl;

        std::this_thread::sleep_for(delay);
    }

    std::cout << "bw#" << id << " is finished..." << std::endl;
}

class BackgroundWork
{
    const size_t id_;

public:
    BackgroundWork(size_t id)
        : id_{id}
    {
    }

    void operator()(const std::string& text, std::chrono::milliseconds delay)
    {
        std::cout << "BW#" << id_ << " has started..." << std::endl;

        for (const auto& c : text)
        {
            std::cout << "BW#" << id_ << ": " << c << std::endl;

            std::this_thread::sleep_for(delay);
        }

        std::cout << "BW#" << id_ << " is finished..." << std::endl;
    }
};

template <typename Container>
void print(const Container& c, const std::string& prefix)
{
    std::cout << prefix << ": [ ";
    for (const auto& item : c)
    {
        std::cout << item << " ";
    }

    std::cout << "]" << std::endl;
}

void run_as_deamon()
{
    std::thread thd(BackgroundWork(99), "deamon", 2000ms);

    thd.detach();

    if (!thd.joinable())
        std::cout << "Thread has been detached..." << std::endl;
}

std::thread spawn_thread(unsigned int id)
{
    std::thread thd{[id]{background_work(id, "spawned thread", 200ms);}};

    return thd;
}

void may_throw(int arg)
{
    if (arg == 13)
        std::runtime_error("Error#13");
}

int main()
{
    std::thread thd1{&background_work, 1, "text", 100ms};
    std::thread thd2 = std::move(thd1);
    std::thread thd3 = spawn_thread(665);
    std::thread thd4 = std::thread{&background_work, 42, "third", 50ms};

    assert(!thd1.joinable());

    std::vector<std::thread> threads(2);

    threads[0] = std::move(thd2);
    threads[1] = spawn_thread(999);
    threads.push_back(std::move(thd3));
    threads.push_back(std::move(thd4));
    threads.emplace_back(&background_work, 4, "TEXT", 145ms);

    may_throw(13);

    for(auto& thd : threads)
        thd.join();

    std::cout << "Logic CPU count: " << std::max(std::thread::hardware_concurrency(), 1u) << "\n";
}
