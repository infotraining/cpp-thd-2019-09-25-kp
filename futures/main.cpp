#include <cassert>
#include <chrono>
#include <functional>
#include <iostream>
#include <string>
#include <thread>
#include <vector>
#include <random>
#include <future>
#include "joining_thread.hpp"

using namespace std::literals;

int calculate_square(int x)
{
    std::cout << "Starting calc for " << x << " in #" << std::this_thread::get_id() << std::endl;

    std::random_device rd;
    std::uniform_int_distribution<> dist(100, 5000);

    std::this_thread::sleep_for(std::chrono::milliseconds(dist(rd)));

    if (x % 13 == 0)
       throw std::runtime_error("Error#13");

    return x * x;
}

using StopToken = std::atomic<bool>;

void save_to_file(const std::string& filename)
{
    std::cout << "Saving to file:" << filename << std::endl;

    for(int i = 0; i < 20; ++i)
    {
        std::this_thread::sleep_for(100ms);
    }

    std::cout << "File saved: " << filename << std::endl;
}

void save_to_file_stopable(const std::string& filename, StopToken& stop_token)
{
    std::cout << "Saving to file:" << filename << std::endl;
    std::cout << "Address of token inside: " << &stop_token << std::endl;

    for(int i = 0; i < 20; ++i)
    {
        std::this_thread::sleep_for(100ms);

        if (stop_token.load())
        {
            std::cout << "Save interrupted: " << filename << std::endl;
            return;
        }
        else
        {
            std::cout << "*";
            std::cout.flush();
        }
    }

    std::cout << "File saved: " << filename << std::endl;
}

class SquareCalculator
{
    std::promise<int> promise_;
public:
    void operator()(int x)
    {
        try
        {
            int result = calculate_square(x);
            promise_.set_value(result);
        }
        catch(...)
        {
            auto eptr = std::current_exception();
            promise_.set_exception(eptr);
        }
    }

    std::future<int> get_future()
    {
        return promise_.get_future();
    }
};

void using_promise()
{
    SquareCalculator sq;
    auto fresult = sq.get_future();

    ext::joining_thread thd{std::ref(sq), 13};

    try
    {
        std::cout << "sq(13) = " << fresult.get() << std::endl;
    }
    catch(const std::runtime_error& e)
    {
        std::cout << e.what() << std::endl;
    }
}

void using_packaged_task()
{
    // 1st way - packaged task
    std::packaged_task<int()> pt1{[] { return calculate_square(8); } };
    std::packaged_task<int(int)> pt2{ &calculate_square };
    std::packaged_task<void()> pt3{ []{ save_to_file("data.txt");} };

    std::future<int> f1 = pt1.get_future();
    std::future<int> f2 = pt2.get_future();
    std::future<void> f3 = pt3.get_future();

    ext::joining_thread thd1{std::move(pt1)}; // execution of pt1 in a new thread
    ext::joining_thread thd2{std::move(pt2), 13}; // execution of pt2 with arg
    ext::joining_thread thd3{std::move(pt3)};

    while(f1.wait_for(100ms) != std::future_status::ready)
    {
        std::cout << "Main thread is waiting for a result..." << std::endl;
    }

    int result1 = f1.get();
    std::cout << "square(8) = " << result1 << std::endl;

    try
    {
        std::cout << "square(11) = " << f2.get() << std::endl;
    }
    catch(const std::runtime_error& e)
    {
        std::cout << "Caught an exception: " << e.what() << std::endl;
    }

    f3.wait();
}

void using_async()
{
    auto f0 = std::async(std::launch::deferred, &save_to_file, "data.txt");

    std::this_thread::sleep_for(5s);

    f0.get();

    std::future<int> f1 = std::async(std::launch::async, []{return calculate_square(42);});

    std::vector<std::future<int>> future_squares;

    for(int i = 1; i < 10; ++i)
        future_squares.push_back(std::async(std::launch::async, &calculate_square, i));

    future_squares.push_back(std::move(f1));

    for(auto& fs : future_squares)
    {
        try
        {
            std::cout << "square = " << fs.get() << std::endl;
        }
        catch(const std::runtime_error& e)
        {
            std::cout << "Caught an excpetion: " << e.what() << std::endl;
        }
    }
}

template <typename Callable>
auto launch_async(Callable&& callable)
{
    using ResultT = decltype(callable());
    std::packaged_task<ResultT()> pt(std::forward<Callable>(callable));
    std::future<ResultT> f = pt.get_future();
    std::thread thd{std::move(pt)};
    thd.detach();
    return f;
}

void wtf()
{
    std::async(std::launch::async, &save_to_file, "1.dat");
    std::async(std::launch::async, &save_to_file, "2.dat");
    std::async(std::launch::async, &save_to_file, "3.dat");
    std::async(std::launch::async, &save_to_file, "4.dat");
}

void wtf_solved()
{
    launch_async([] { save_to_file("1.dat");});
    launch_async([] { save_to_file("2.dat");});

    StopToken stop{false};
    auto f = launch_async([&stop] { save_to_file_stopable("3.dat", stop);});

    std::this_thread::sleep_for(500ms);
    stop.store(true);
    f.wait();
}

int main()
{
    //using_packaged_task();
    //using_promise();
    //using_async();
    wtf_solved();

    std::this_thread::sleep_for(10s);
}
