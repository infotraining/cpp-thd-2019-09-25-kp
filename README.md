## Multithreading in C++

### Additonal information

#### Doc

* https://infotraining.bitbucket.io/cpp-thd/


#### login and password for VM:

```
dev  /  pwd
```

#### reinstall VBox addon

```
sudo /etc/init.d/vboxadd setup
```

#### proxy settings

We can add them to `.profile`

```
export http_proxy=http://10.144.1.10:8080
export https_proxy=https://10.144.1.10:8080
```

#### vcpkg settings

Add to `.profile`

```
export VCPKG_ROOT="/usr/share/vcpkg" 
export CC="/usr/bin/gcc-9"
export CXX="/usr/bin/g++-9"
```

#### GIT

```
git clone https://bitbucket.org/infotraining/cpp-thd-2019-09-25-kp
```

#### Links

* [git cheat sheet](http://www.cheat-sheets.org/saved-copy/git-cheat-sheet.pdf)

* [Codinghorror - Infinite space between words](https://blog.codinghorror.com/the-infinite-space-between-words/)

* [compiler explorer](https://gcc.godbolt.org/)

* https://infotraining.bitbucket.io/cpp-17/parallel-stl.html

* https://www.youtube.com/watch?v=ARYP83yNAWk

## Ankieta

* https://www.infotraining.pl/ankieta/cpp-thd-2019-09-25-kp
