#include <cassert>
#include <chrono>
#include <functional>
#include <iostream>
#include <string>
#include <thread>
#include <vector>

using namespace std::literals;

void hello(int& call_counter)
{
    std::string text = "Hello Concurrent World";

    for (const auto& c : text)
    {
        std::cout << c << " ";
        std::this_thread::sleep_for(100ms);
        std::cout.flush();
    }

    std::cout << std::endl;
    ++call_counter;
}

void background_work(size_t id, const std::string& text, std::chrono::milliseconds delay)
{
    std::cout << "bw#" << id << " has started..." << std::endl;

    for (const auto& c : text)
    {
        std::cout << "bw#" << id << ": " << c << std::endl;

        std::this_thread::sleep_for(delay);
    }

    std::cout << "bw#" << id << " is finished..." << std::endl;
}

class BackgroundWork
{
    const size_t id_;

public:
    BackgroundWork(size_t id)
        : id_{id}
    {
    }

    void operator()(const std::string& text, std::chrono::milliseconds delay)
    {
        std::cout << "BW#" << id_ << " has started..." << std::endl;

        for (const auto& c : text)
        {
            std::cout << "BW#" << id_ << ": " << c << std::endl;

            std::this_thread::sleep_for(delay);
        }

        std::cout << "BW#" << id_ << " is finished..." << std::endl;
    }
};

template <typename Container>
void print(const Container& c, const std::string& prefix)
{
    std::cout << prefix << ": [ ";
    for (const auto& item : c)
    {
        std::cout << item << " ";
    }

    std::cout << "]" << std::endl;
}

void run_as_deamon()
{
    std::thread thd(BackgroundWork(99), "deamon", 2000ms);

    thd.detach();

    if (!thd.joinable())
        std::cout << "Thread has been detached..." << std::endl;
}

int main()
{
    int hello_call_counter = 0;

    std::thread thd1{&hello, std::ref(hello_call_counter)};

    BackgroundWork bw1{1};
    std::thread thd2{bw1, "multithreading", 100ms};

    std::thread thd3{&background_work, 42, "text", 205ms};

    const int id = 665;
    std::thread thd4{[id]() { background_work(id, "TEXT", 3000ms); } };

    thd4.join();
    std::cout << "THD4 finished." << std::endl;
    thd1.join();
    std::cout << "THD1 finished." << std::endl;
    thd2.join();
    std::cout << "THD2 finished." << std::endl;
    thd3.detach();

    std::thread thd_empty;
    std::cout << thd_empty.get_id() << " - " << thd_empty.joinable() << std::endl;
    std::cout << thd3.get_id() << " - " << thd3.joinable() << std::endl;

    std::cout << "hello_call_counter: " << hello_call_counter << std::endl;

    std::cout << "--------------------------\n";

    const std::vector<int> data = { 1, 2, 3, 4, 5, 6, 7 };

    std::vector<int> target1;
    std::vector<int> target2;

    std::thread cpy_thd1{[&data, &target1] { target1.assign(begin(data), end(data));}};
    std::thread cpy_thd2{[&data, &target2] { target2.assign(begin(data), end(data));}};

    cpy_thd1.join();
    cpy_thd2.join();

    print(target1, "target1");
    print(target2, "target2");

    std::cout << "--------------------\n";

    int x = 10;
    std::string str = "text";

    auto l1 = [&x, str]() { std::cout << "l1 - " << str << "\n"; ++x; };
    //auto l1_explain = Lambda_3475127834523{};
    l1();
    //l1_explain();

    int x = 10;
    double pi = 3.14;
    std::string str = "text";

    auto l2 = [&x, pi, &str] { std::cout << x << " " << pi << " " << str << "\n"; };
    auto l3 = [=, &pi] { std::cout << x << " " << pi << " " << str << "\n"; };
    auto l4 = [&, x] { std::cout << x << " " << pi << " " << str << "\n"; };
}

template <typename LL>
struct LambdaWrapper
{
    int counter = 0;
    LL& ll_;

    LambdaWrapper(LL& ll) : ll_{ll}
    {}

    auto operator()()
    {
        ll_();
        ++counter;
    }
};

class Lambda_3475127834523
{
    int& x;
    std::string str;
public:
    auto operator()() const { std::cout << "l1 - " << str << "\n"; ++x; }
};

void magic_lambda()
{
    auto l = []() { std::cout << "ml\n"; };

    LambdaWrapper<decltype(l)> lw(l);
    lw();
    lw();
    std::cout << lw.counter << "\n";
}
