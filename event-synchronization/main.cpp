#include <cassert>
#include <chrono>
#include <functional>
#include <iostream>
#include <string>
#include <thread>
#include <vector>
#include <random>
#include <algorithm>
#include <atomic>
#include <condition_variable>
#include "joining_thread.hpp"

using namespace std::literals;

namespace atomics
{
class Data
{
    std::vector<int> data_;
    std::atomic<bool> is_ready_{false};
public:
    void read()
    {
        std::cout << "Start reading..." << std::endl;

        data_.resize(100);

        std::random_device rd;
        std::generate_n(data_.begin(), 100, [&rd] { return rd() % 1000;});
        std::this_thread::sleep_for(2500ms);

        is_ready_.store(true, std::memory_order_release); //XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX

        std::cout << "End of reading..." << std::endl;
    }

    void process(int id)
    {
        while(!is_ready_.load(std::memory_order_acquire)) //XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
        {}

        long sum = std::accumulate(data_.begin(), data_.end(), 0L);

        std::cout << "Id: " << id << "; Sum: " << sum << std::endl;
    }
};
}

class Data
{
    std::vector<int> data_;
    bool is_ready_{false};
    std::mutex mtx_is_ready_;
    std::condition_variable cv_is_ready_;
public:
    void read()
    {
        std::cout << "Start reading..." << std::endl;

        data_.resize(100);

        std::random_device rd;
        std::generate_n(data_.begin(), 100, [&rd] { return rd() % 1000;});
        std::this_thread::sleep_for(2500ms);

        {
            std::lock_guard lk{mtx_is_ready_};
            is_ready_ = true;
        }

        cv_is_ready_.notify_all();

        std::cout << "End of reading..." << std::endl;
    }

    void process(int id)
    {
        {
            std::unique_lock lk{mtx_is_ready_};
            cv_is_ready_.wait(lk, [this] { return is_ready_;} );
        }

        long sum = std::accumulate(data_.begin(), data_.end(), 0L);

        std::cout << "Id: " << id << "; Sum: " << sum << std::endl;
    }
};

int main()
{
    Data data;
    ext::joining_thread thd_producer{[&data] { data.read();}};

    ext::joining_thread thd_consumer1{[&data] {data.process(1);}};
    ext::joining_thread thd_consumer2{[&data] {data.process(2);}};
}
