#include <cassert>
#include <chrono>
#include <functional>
#include <iostream>
#include <string>
#include <thread>
#include <vector>
#include <type_traits>

using namespace std::literals;

void background_work(size_t id, const std::string& text, std::chrono::milliseconds delay)
{
    std::cout << "bw#" << id << " has started..." << std::endl;

    for (const auto& c : text)
    {
        std::cout << "bw#" << id << ": " << c << std::endl;

        std::this_thread::sleep_for(delay);
    }

    std::cout << "bw#" << id << " is finished..." << std::endl;
}

void may_throw(int arg)
{
    if (arg == 13)
        throw std::runtime_error("Error#13");
}

namespace Simple
{
    class JoiningThread
    {
        std::thread& thd_;
    public:
        JoiningThread(std::thread& thd) : thd_{thd}
        {}

        JoiningThread(const JoiningThread&) = delete;
        JoiningThread& operator=(const JoiningThread&) = delete;
        JoiningThread(JoiningThread&&) = delete;
        JoiningThread& operator=(JoiningThread&&) = delete;

        ~JoiningThread()
        {
            if (thd_.joinable())
                thd_.join();
        }
    };
}

// template  variable
template <typename T1, typename T2>
constexpr bool is_similar_v = std::is_same<std::decay_t<T1>, std::decay_t<T2>>::value;

class JoiningThread
{
    std::thread thd_;

public:
    template <typename Callable, typename... Args,
        typename = std::enable_if_t<!is_similar_v<Callable, JoiningThread>>>
    JoiningThread(Callable&& callable, Args&&... args)
        : thd_{std::forward<Callable>(callable), std::forward<Args>(args)...}
    {}

    JoiningThread(const JoiningThread&) = delete;
    JoiningThread& operator=(const JoiningThread&) = delete;

    JoiningThread(JoiningThread&&) = default;
    JoiningThread& operator=(JoiningThread&&) = default;

    std::thread& get()
    {
        return thd_;
    }

    void join()
    {
        thd_.join();
    }

    void detach()
    {
        thd_.detach();
    }

    bool joinable() const
    {
        return thd_.joinable();
    }

    ~JoiningThread()
    {
        if (thd_.joinable())
            thd_.join();
    }
};

template <typename T, typename = std::enable_if_t<std::is_integral_v<T>>>
void calc(T value)
{
    std::cout << "Integral impl: " << value << "\n";
}

template <typename T, typename = std::enable_if_t<!std::is_integral_v<T>>, typename = void>
void calc(T value)
{
    std::cout << "Alt impl: " << value << "\n";
}

int main() try
{
    calc(3.14);
    calc(42);

    std::string text = "work";

    JoiningThread j_thd{&background_work, 1, text, 200ms};
    //JoiningThread j_thd2{j_thd};

    may_throw(13);
}
catch(const std::exception& e)
{
    std::cout << "caught an exception: " << e.what() << std::endl;
}
