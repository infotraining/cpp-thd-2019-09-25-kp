#include <cassert>
#include <chrono>
#include <functional>
#include <iostream>
#include <string>
#include <thread>
#include <vector>
#include "joining_thread.hpp"
#include "thread_safe_queue.hpp"

using namespace std::literals;

void background_work(size_t id, std::chrono::milliseconds delay)
{
    std::cout << "bw#" << id << " has started..." << std::endl;

    std::this_thread::sleep_for(delay);

    std::cout << "bw#" << id << " is finished..." << std::endl;
}

void foo(int x)
{
    std::cout << "foo(" << x << ")\n";
}

class Foo
{
public:
    void operator()(int x)
    {
        std::cout << "Foo::op(" << x << ")\n";
    }
};

void function_explain()
{
    void (*ptr_fun)(int) = foo;
    ptr_fun(13);

    Foo foo_obj;
    foo_obj(13);

    auto lfoo = [](int x) { std::cout << "lfoo(" << x << ")\n"; };
    lfoo(13);

    std::function<void(int)> f;
    f = ptr_fun;
    f(42);

    f = foo_obj;
    f(42);

    f = lfoo;
    f(42);
}


class ThreadPool
{
public:
    using Task = std::function<void()>;

private:
    ThreadSafeQueue<Task> tasks_;
    std::vector<ext::joining_thread> threads_;

    const std::nullptr_t end_of_work_{};

    void run()
    {
        while(true)
        {
            Task task;
            tasks_.pop(task);

            if (task == end_of_work_)
                return;

            task(); // execute submitted task
        }
    }

public:
    explicit ThreadPool(unsigned int size)
        : threads_(size)
    {
        for(unsigned int i = 0; i < size; ++i)
            threads_[i] = ext::joining_thread([this] { run(); });
    }

    ~ThreadPool()
    {
        for(size_t i = 0; i < threads_.size(); ++i)
            tasks_.push(end_of_work_); // sending poisoning pill
    }

    ThreadPool(const ThreadPool&) = delete;
    ThreadPool& operator=(const ThreadPool&) = delete;

    template <typename Callable>
    void submit(Callable&& task)
    {
        tasks_.push(std::forward<Callable>(task));
    }

    void submit_slower(Task task) // std::function will be copied many times
    {
        tasks_.push(task);
    }
};

int main()
{
    std::cout << "Main thread starts..." << std::endl;

    ThreadPool thread_pool(6);

    thread_pool.submit([] { background_work(1, 200ms);});
    thread_pool.submit([] { background_work(2, 200ms);});

    for(unsigned int i = 3; i < 20; ++i)
        thread_pool.submit([i] { background_work(i, std::chrono::milliseconds(i * 50));});

    std::cout << "Main thread ends..." << std::endl;
}
