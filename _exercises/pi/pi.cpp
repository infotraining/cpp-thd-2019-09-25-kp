#include <atomic>
#include <chrono>
#include <iostream>
#include <random>
#include <thread>
#include <random>
#include <future>

using namespace std;

uint64_t calc_hits(uint64_t N)
{
    std::random_device rd;
    std::seed_seq seed{ rd(), rd(), rd(), rd(), rd(), rd() };
    std::mt19937 rnd_gen(seed);
    std::uniform_real_distribution<double> dist(0, 1.0);

    uint64_t counter{};

    for (uint64_t n = 0; n < N; ++n)
    {
        double x = dist(rnd_gen);
        double y = dist(rnd_gen);
        if (x * x + y * y < 1)
            counter++;
    }

    return counter;
}

int main()
{
    uint64_t N = 100'000'000;
    uint64_t counter = 0;

    {
        std::random_device rd;
        std::seed_seq seed{ rd(), rd(), rd(), rd(), rd(), rd() };
        std::mt19937 rnd_gen(seed);
        std::uniform_real_distribution<double> dist(0, 1.0);


        cout << "Pi calc!" << endl;
        auto start = chrono::high_resolution_clock::now();

        for (uint64_t n = 0; n < N; ++n)
        {
            double x = dist(rnd_gen);
            double y = dist(rnd_gen);
            if (x * x + y * y < 1)
                counter++;
        }
        auto end = chrono::high_resolution_clock::now();
        cout << "Pi = " << double(counter) / double(N) * 4 << endl;
        cout << "Elapsed = ";
        cout << chrono::duration_cast<chrono::milliseconds>(end - start).count();
        cout << " ms" << endl;

        std::cout << "\n\n-----------------------------\n\n";
    }

    {
        cout << "Pi with futures!" << endl;
        auto start = chrono::high_resolution_clock::now();

        auto tasks_count = std::max(std::thread::hardware_concurrency(), 1u);

        std::vector<std::future<uint64_t>> fhits;
        for (size_t i = 0; i < tasks_count; ++i)
        {
            fhits.push_back(std::async(std::launch::async, &calc_hits, N/tasks_count));
        }

        auto counter =
                std::accumulate(fhits.begin(), fhits.end(), 0ull,
                                [](uint64_t res, auto& f) { return res + f.get();});

        auto end = chrono::high_resolution_clock::now();
        cout << "Pi = " << double(counter) / double(N) * 4 << endl;
        cout << "Elapsed = ";
        cout << chrono::duration_cast<chrono::milliseconds>(end - start).count();
        cout << " ms" << endl;
    }

    return 0;
}
