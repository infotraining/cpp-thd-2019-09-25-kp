#include <iostream>
#include <thread>
#include <mutex>

class BankAccount
{
    const int id_;
    double balance_;
    mutable std::recursive_mutex mtx_;

public:
    BankAccount(int id, double balance)
        : id_(id)
        , balance_(balance)
    {
    }

    void print() const
    {
        std::cout << "Bank Account #" << id_ << "; Balance = " << balance() << std::endl;
    }

    void transfer_eventual_consistency(BankAccount& to, double amount)
    {
        this->withdraw(amount);
        //xxxxxxxxx
        to.deposit(amount);
    }

    void transfer(BankAccount& to, double amount)
    {
#if __cplusplus < 201703L
        std::unique_lock<std::recursive_mutex> lk_from{mtx_, std::defer_lock};
        std::unique_lock<std::recursive_mutex> lk_to{to.mtx_, std::defer_lock};
        std::lock(lk_from, lk_to);
#else
        std::scoped_lock lk(mtx_, to.mtx_);
#endif

        withdraw(amount);
        to.deposit(amount);
    }


    void withdraw(double amount)
    {
        std::lock_guard lk{mtx_};
        balance_ -= amount;        
    }

    void deposit(double amount)
    {
        std::lock_guard lk{mtx_};
        balance_ += amount;       
    }

    int id() const
    {
        return id_;
    }

    double balance() const
    {
        std::lock_guard lk{mtx_};
        return balance_;
    }

    void lock()
    {
        mtx_.lock();
    }

    void unlock()
    {
        mtx_.unlock();
    }
};

void make_deposits(BankAccount& ba, double amount, size_t counter)
{
    for(size_t i = 0; i < counter; ++i)
        ba.deposit(amount);
}

void make_withdraws(BankAccount& ba, double amount, size_t counter)
{
    for(size_t i = 0; i < counter; ++i)
        ba.withdraw(amount);
}

void make_transfers(BankAccount& from, BankAccount& to, double amount, size_t counter)
{
    for(size_t i = 0; i < counter; ++i)
        from.transfer(to, amount);
}

int main()
{
    BankAccount ba1(1, 10'000);
    BankAccount ba2(2, 10'000);

    std::thread thd1{&make_deposits, std::ref(ba1), 1.0, 1'000'000};
    std::thread thd2{&make_withdraws, std::ref(ba1), 1.0, 1'000'000};
    std::thread thd3{&make_transfers, std::ref(ba1), std::ref(ba2), 1.0, 10'000};

    auto temp_b1_balance = ba1.balance();

    thd1.join();
    thd2.join();
    thd3.join();

    ba1.print();
    ba2.print();

    std::cout << "\n-------------------------\n";

    std::thread thd4{&make_transfers, std::ref(ba1), std::ref(ba2), 1.0, 10'000};
    std::thread thd5{&make_transfers, std::ref(ba2), std::ref(ba1), 1.0, 10'000};

    {
        std::lock_guard lk{ba1};
        ba1.deposit(100.0);
        ba1.withdraw(99.0);
    }

    thd4.join();
    thd5.join();

    ba1.print();
    ba2.print();
}
